const express = require('express');
const app = express();
const PORT = 3000;
const user = require('./user/user.json');
const bcrypt = require('bcrypt');

app.use(express.json());
app.use(express.static('views'));

app.get('/', (req, res) => {
  res.sendFile('./views/home/index.html', { root: __dirname });
});

app.get('/game', (req, res) => {
  res.sendFile('./views/game/index.html', { root: __dirname });
});

// user
app.get('/user', (req, res) => {
  res.status(200).json(user);
});

app.post('/user', async (req, res) => {
  try {
    const salt = await bcrypt.genSalt();
    const hashedPassword = await bcrypt.hash(req.body.password, 10);

    const users = { name: req.body.name, password: hashedPassword };
    user.push(users);
    res.status(201).send('CREATED!');
  } catch {
    res.status(500).send('internal server error');
  }
});

app.post('/user/login', async (req, res) => {
  const userLogin = user.find((userLogin) => userLogin.name === req.body.name);
  if (userLogin == null) {
    return res.status(400).send('Cannot Find User');
  }
  try {
    if (await bcrypt.compare(req.body.password, userLogin.password)) res.send('success');
    res.send('SUCCESS');
  } catch {
    res.send('Not Allowed!');
  }
});

app.use('/', (req, res) => {
  res.status(404);
});

app.listen(PORT, () => console.log(`Server Running on http://localhost:${PORT}`));
