// reload function
const reload = document.querySelector('.refresh');
reload.addEventListener('click', function(){
versus.classList.remove('hasil')
versus.innerHTML = ('VS')
versus.style.color = '#bd0000';
versus.style.fontSize = '100px'
const hapus = document.querySelectorAll('.batu, .gunting, .kertas, .batu-com, .kertas-com, .gunting-com'); 
for(let i = 0; i<= hapus.length; i++){
  hapus[i].style.backgroundColor= '';  
}


})

// tampilan VS menang, kalah dll
let versus = document.getElementById('versus');
function tampilhasil(n) {
  versus.innerHTML = `${n}`;
  versus.classList.add('hasil');
  versus.style.color = 'white';
  versus.style.fontSize = '40px';
}

// COM Logic
function randomCom() {
  let comp = Math.random();
  if (comp < 0.334) return 'batu';
  if (comp > 0.667) return 'kertas';
  return 'gunting';
}

// click effect
function klik(n) {
  const pilihan = document.getElementsByClassName(`${n}`);
  pilihan[0].style.backgroundColor = '#c4c4c4';
  
}
//  game rules
function hasil(comp, player) {
  if (player == comp)
    // "DRAW" nanti diganti dengan css draw
    return 'DRAW';
  if (player == 'batu') return comp == 'kertas' ? 'COM MENANG' : 'PLAYER MENANG';
  if (player == 'gunting') return comp == 'batu' ? 'COM MENANG' : 'PLAYER MENANG';
  if (player == 'kertas') return comp == 'gunting' ? 'COM MENANG' : 'PLAYER MENANG';
}

// GAME
function inputan(n) {
  const pilihanCOM = randomCom();
  const pilihanPLAYER = n.className;
  const hasilAkhir = hasil(pilihanCOM, pilihanPLAYER);
  const highlight = document.getElementsByClassName(`${pilihanCOM}-com`)
  highlight[0].style.backgroundColor = '#c4c4c4';
  klik(pilihanPLAYER);
  tampilhasil(hasilAkhir);
  console.log(`=================================`);
  console.log(`player memilih: ${pilihanPLAYER}`);
  console.log(`comp memilih  : ${pilihanCOM}`);
  console.log(`hasil         : ${hasilAkhir}`);
  console.log(`=================================`);
}

const pBatu = document.querySelector('.batu');
pBatu.addEventListener('click', function () {
  inputan(pBatu);
});
const pKertas = document.querySelector('.kertas');
pKertas.addEventListener('click', function () {
  inputan(pKertas);
});
const pGunting = document.querySelector('.gunting');
pGunting.addEventListener('click', function () {
  inputan(pGunting);
});
